#include <stddef.h>

typedef struct Request_Info {
    // url decoded query string:
    const char *query_string_decoded;

    const char *script_path;
    const char *path_info;
    const char *server_name;
    const char *server_port;
    const char *remote_addr;

    // tls client hash with any 'SHA256:' prefix stripped.
    // At least with molly-brown and GLV-1.12556, this will be the sha256 hash 
    // of the certificate as a 64 character hex string.
    const char *tls_client_hash;

    const char *tls_client_issuer;
    const char *tls_client_issuer_cn;
    const char *tls_client_subject;
    const char *tls_client_subject_cn;
} Request_Info;

typedef void write_response_cb(const void *object, const char* buf, size_t n);
typedef void respond_cb(void *object, const Request_Info *request_info,
        int socket);

/* Create a socket at `socket_path` and accept valid SCGI requests with empty 
 * body; for each such, `respond` will be called with `respond_object` and a 
 * `Request_Info` struct filled in according to the request headers. This 
 * should (optionally in a new thread) write its response to the fd `socket` 
 * and then close `socket`. */
void runSCGI(const char *socket_path, respond_cb respond, void *respond_object);
