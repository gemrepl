#!/bin/sh
# Run instances of the gemini client diohsc for recursive fun, setting a cpu 
# time ulimit and passing options which prevent diohsc writing to the 
# filesystem or spawning subcommands.
# Sadly, with the way ghc works, it seems not to be possible to use ulimit to 
# limit RAM use.
ulimit -t 300

stdbuf -i0 -o0 diohsc-gemrepl -C -bprg -d /dev/null gemini.circumlunar.space
