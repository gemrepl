#!/bin/sh
# A very simple example to demonstrate mixing options given by links and 
# free-form input.

function readline {
    echo -n '<' >&3
    read line
    ret=$?
    echo -n '>' >&3
    echo "$line"
    return $ret
}

while true; do
    cat <<EOF
What's your favourite colour?
=> ?red Red
=> ?green Green
=> ?octarine Octarine
=> ?!? [Something else]
EOF
    colour="$(readline)"
    cat <<EOF
Hmm... $colour, eh? It has its virtues, I suppose.
But are you sure really sure $colour is your favourite?
=> ?yes Yes
=> ?no No
EOF
    while conf="$(readline)"; do
        if [ "$conf" == yes ]; then
            echo "Fine."
            exit
        elif [ "$conf" == no ]; then
            echo "Hmm, let's try this again then."
            break
        else
            echo "Pardon? Was that yes or no?"
        fi
    done
done
