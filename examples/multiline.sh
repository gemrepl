#!/bin/sh
# Showing one possible way of doing multiline input

function readline {
    echo -n '<' >&3
    read line
    ret=$?
    echo -n '>' >&3
    echo "$line"
    return $ret
}

while readline; do true; done | (
    echo "Enter your message, ending with a line consisting of a single dot."
    # dot-stuff as in RFC 5321: to input a line starting with a literal dot, 
    # use two dots.
    message="$(sed -n '/^\.$/q;s/^\.//;p')"
    echo "You wrote the following $(echo "$message"|wc -l) line(s):"
    echo "$message"
)
