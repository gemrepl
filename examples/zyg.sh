#!/bin/sh
# Run z-machine interpreter, allowing the user to save their game in a 
# directory specific to their client cert.

var=/var/gemini/zyg/

function readline {
    echo -n '<' >&3
    read line
    ret=$?
    echo -n '>' >&3
    echo "$line"
    return $ret
}

[ -n "$TLS_CLIENT_HASH" ] || exit 1

if [ -n "$SPAWN_PARAMETER" ]; then
    sanitised="$(tr -dc [:alnum:]_ <<< "$SPAWN_PARAMETER")"
    gamefile="$var/games/$sanitised"
    if ! ( [ -n "$sanitised" ] && [ -e "$gamefile" ] ); then
        echo "Unknown game: $sanitised"
        exit 1
    fi
else
    echo "Choose your poison:"
    ls -1 "$var/games/"

    while input="$(readline)"; do
        sanitised="$(tr -dc [:alnum:]_ <<< "$input")"
        gamefile="$var/games/$sanitised"
        if [ -n "$sanitised" ] && [ -e "$gamefile" ]; then
            break;
        fi
        echo "Please enter one of the listed games"
    done
fi

echo "Starting $sanitised. Savefiles will be preserved."
echo

savesdir="$var/$TLS_CLIENT_HASH/$sanitised"

mkdir -p "$savesdir" || exit 1
cd "$savesdir" || exit 1

# Impose some limits to reduce potential for abuse
ulimit -t 1200
ulimit -f 5000
ulimit -v 20000

if [[ "$(realpath "$gamefile")" =~ \.gblorb$ ]]; then
    stdbuf -i0 -o0 cheapgit-zyg "$gamefile"
else
    # using the z-machine interpreter infuse, which has a nice stdio interface: 
    # https://gitlab.com/monkeymind/infuse
    # Lightly hacked to improve output and to write to stderr when prompting
    stdbuf -i0 -o0 infuse "$gamefile"

    # Previous attempt: using dfrotz, but it has some problems.
    #dfrotz -m -Z0 -R. "$gamefile"
fi
