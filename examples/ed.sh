#!/bin/sh
# Run ed in restricted mode, allowing the user to edit files in a directory 
# specific to their client cert.

var=/var/gemini/ed/

[ -n "$TLS_CLIENT_HASH" ] || exit 1

mkdir -p "$var/$TLS_CLIENT_HASH" || exit 1
cd "$var/$TLS_CLIENT_HASH" || exit 1

# Impose some severe limits to reduce potential for abuse
ulimit -n 10
ulimit -t 300
ulimit -f 1000
ulimit -v 5000

touch sandbox
ed -r -v sandbox
