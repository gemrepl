#!/bin/sh
gemrepl -s /tmp/gemrepl_choice ./choice.sh&
gemrepl --format=pre -m 50 -t 10000 -T 10000 -s /tmp/gemrepl_diohsc ./diohsc.sh&
gemrepl --format=pre -t 300 -T 100 -s /tmp/gemrepl_ed ./ed.sh&
gemrepl --format=pre -m 50 -s /tmp/gemrepl_zyg ./zyg.sh&
gemrepl -s /tmp/gemrepl_multiline ./multiline.sh&
