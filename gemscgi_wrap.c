#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "gemscgi.h"

void respond(void *object, const Request_Info *request_info, int socket)
{
    char **args = object;

    char tlsenv[64+16+1];
    char queryenv[1024+13+1];
    char pathenv[1024+10+1];
    if (request_info->tls_client_hash != NULL) {
        snprintf(tlsenv, sizeof(tlsenv), "TLS_CLIENT_HASH=%s", request_info->tls_client_hash);
    }
    if (request_info->query_string_decoded != NULL) {
        snprintf(queryenv, sizeof(queryenv), "QUERY_STRING=%s", request_info->query_string_decoded);
    }
    if (request_info->path_info != NULL) {
        snprintf(pathenv, sizeof(pathenv), "PATH_INFO=%s", request_info->path_info);
    }

    const pid_t pid = fork();
    if (pid == -1) {
        perror("fork");
        return;
    } else if (pid > 0) {
        close(socket);
        return;
    }

    putenv(tlsenv);
    putenv(queryenv);
    putenv(pathenv);

    dup2(socket, 1);
    setbuffer(stdout, NULL, 0);

    execvp(args[0], args);
    exit(1);
}

static void usage()
{
    printf("Usage: gemscgi_wrap PATH COMMAND [ARG]...\n");
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        usage();
        exit(1);
    }

    /* Prevent zombies (on modern posix) */
    signal(SIGCHLD, SIG_IGN);

    runSCGI(argv[1], respond, &argv[2]);
}
