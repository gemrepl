/* Copyright 2021, Martin Bays <mbays@sdf.org>
 * SPDX-License-Identifier: GPL-3.0-or-later */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#include "gemscgi.h"

/* Decode percent-escapes in place */
static void urldecode(char *p) {
    char *w = p;
    bool escaped = false;

    while (*p) {
        if (*p == '%') {
            escaped = true;
            ++p;
            if (sscanf(p, "%2hhx", w) == 1) {
                ++w;
                p+=2;
            }
        } else {
            if (escaped) *w = *p;
            ++p;
            ++w;
        }
    }
    *w = 0;
}

#define MAX_REQUEST_SZ 4096
/* Parse SCGI request with null body from fd s and place gemini-relevant 
 * header values in request_info.
 * Return false on failure. */
static bool parse_SCGI(int s, respond_cb respond, void *respond_object)
{
    Request_Info request_info = { };
    char buf[MAX_REQUEST_SZ];
    int r = 0;
    while (1) {
        int r2 = read(s, buf + r, MAX_REQUEST_SZ - r);
        if (r2 <= 0) {
            perror("read");
            return false;
        }
        r += r2;

        int sz;
        if (sscanf(buf, "%u:", &sz) == 1) {
            char buf2[16];
            snprintf(buf2, 16, "%u:,", sz);
            const int full_sz = sz + strlen(buf2);

            if (full_sz > MAX_REQUEST_SZ) {
                fprintf(stderr, "Bad request: Oversized, %u\n", sz);
                return false;
            }

            if (r == full_sz) {
                break;
            }

            if (r > full_sz) {
                fprintf(stderr, "Bad request: non-empty body.\n");
                return false;
            }
        }
    }

    const char* end = &buf[r-1];

    if (r <= 2) {
        /* Null request */
    } else if (buf[r-1] != ',' || buf[r-2] != 0) {
        fprintf(stderr, "Bad request: improperly terminated header.\n");
        return false;
    } else {
        /* Parse headers */
        char *p = buf;
        while (*p != ':') p++;
        p++;

        while (p != end) {
            const char* key = p;
            while (*p) ++p;
            ++p;
            if (p == end) {
                fprintf(stderr, "Bad request: key with no value.\n");
                return false;
            }

            char* val = p;
            while (*p) ++p;
            ++p;

            if (strcmp(key, "QUERY_STRING") == 0) {
                urldecode(val);
                request_info.query_string_decoded = val;
            } else if (strcmp(key, "SCRIPT_PATH") == 0) {
                request_info.script_path = val;
            } else if (strcmp(key, "PATH_INFO") == 0) {
                request_info.path_info = val;
            } else if (strcmp(key, "SERVER_NAME") == 0) {
                request_info.server_name = val;
            } else if (strcmp(key, "SERVER_PORT") == 0) {
                request_info.server_port = val;
            } else if (strcmp(key, "REMOTE_ADDR") == 0) {
                request_info.remote_addr = val;
            } else if (strcmp(key, "TLS_CLIENT_ISSUER") == 0) {
                request_info.tls_client_issuer = val;
            } else if (strcmp(key, "TLS_CLIENT_ISSUER_CN") == 0) {
                request_info.tls_client_issuer_cn = val;
            } else if (strcmp(key, "TLS_CLIENT_SUBJECT") == 0) {
                request_info.tls_client_subject = val;
            } else if (strcmp(key, "TLS_CLIENT_SUBJECT_CN") == 0) {
                request_info.tls_client_subject_cn = val;
            } else if (strcmp(key, "TLS_CLIENT_HASH") == 0) {
                if (strncmp(val, "SHA256:", 7) == 0) val += 7;
                request_info.tls_client_hash = val;
            }
        }
    }

    respond(respond_object, &request_info, s);

    return true;
}

void runSCGI(const char *socket_path, respond_cb respond, void *respond_object)
{
    // Unix socket gubbins based on
    // https://beej.us/guide/bgipc/html/multi/unixsock.html

    const int s = socket(AF_UNIX, SOCK_STREAM, 0);
    if (s == -1) {
        perror("socket");
        exit(1);
    }

    struct sockaddr_un local;
    local.sun_family = AF_UNIX;
    strcpy(local.sun_path, socket_path);
    unlink(local.sun_path);
    if (bind(s, (struct sockaddr *)&local,
                strlen(local.sun_path) + sizeof(local.sun_family)) == -1) {
        perror("bind");
        exit(1);
    }

    if (listen(s, 20) == -1) {
        perror("listen");
        exit(1);
    }

    while (1) {
        const int s2 = accept(s, NULL, NULL);
        if (s2 == -1) {
            perror("accept");
            exit(1);
        }

        parse_SCGI(s2, respond, respond_object);
    }
}
