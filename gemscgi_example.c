#include <string.h>
#include <unistd.h>

#include "gemscgi.h"

void respond(void *object, const Request_Info *request_info, int socket)
{
#define put(s) write(socket, s, strlen(s))
    put("20\r\n");
    if (request_info->query_string_decoded != NULL) {
        put(request_info->query_string_decoded);
        put("\r\n");
    }
    if (request_info->path_info != NULL) {
        put(request_info->path_info);
        put("\r\n");
    }
    if (request_info->script_path != NULL) {
        put(request_info->script_path);
        put("\r\n");
    }
    if (request_info->tls_client_hash != NULL) {
        put(request_info->tls_client_hash);
        put("\r\n");
    }
}

int main(int argc, const char **argv)
{
    runSCGI("/tmp/gemscgi_testsocket", respond, NULL);
}
