/* Copyright 2021, Martin Bays <mbays@sdf.org>
 * SPDX-License-Identifier: GPL-3.0-or-later */
#include <fcntl.h>
#include <getopt.h>
#include <poll.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "gemscgi.h"

/* If you increase this too far, you may run into file descriptor limits */
#define MAX_CHILDREN 256

#define SESSION_ID_LEN 8

typedef struct Child {
    bool exists;
    bool newborn;
    pthread_mutex_t *mutex; // initialised if child->exists, maybe also if not
    char sess_id[SESSION_ID_LEN];
    char owner[64];
    uint64_t last_active;
    pid_t pid;
    int in;
    int out;
    int flag;
    bool reading;
    int serial;

    bool no_link;
    bool always_link;
    bool plain;
} Child;

typedef enum output_format
    { gemtext
    , pre
    , unwrapped
    , raw
} output_format;

typedef struct State {
    const char *command;
    char *const *args;
    output_format format;

    int max_children;
    int read_timeout;
    int pause_timeout;
    bool no_link;
    bool always_link;
    bool single_session;

    int num_children;
    Child children[MAX_CHILDREN];
} State;

static bool spawn(const char *command, char *const *args, const char *query,
        Child *child, int socket)
{
    int infds[2], outfds[2], flagfds[2];
    if (pipe(infds) == -1 || pipe(outfds) == -1 || pipe(flagfds) == -1) {
        perror("pipe");
        return false;
    }

    const pid_t pid = fork();
    if (pid == -1) {
        perror("fork");
        return false;
    }

    if (pid == 0) {
        // child
        close(socket);
        close(infds[1]);
        close(outfds[0]);
        close(flagfds[0]);
        dup2(infds[0], 0);
        dup2(outfds[1], 1);
        dup2(outfds[1], 2);
        dup2(flagfds[1], 3);
        setbuffer(stdin, NULL, 0);
        setbuffer(stdout, NULL, 0);
        setbuffer(fdopen(3, "w"), NULL, 0);
        setsid();

        char tlsenv[64+16+1];
        snprintf(tlsenv, 64+16+1, "TLS_CLIENT_HASH=%s", child->owner);
        putenv(tlsenv);

        if (query != NULL) {
            char qenv[1024+16+1];
            snprintf(qenv, 1024+16+1, "SPAWN_PARAMETER=%s", query);
            putenv(qenv);
        }

        execvp(command, args);
        exit(1);
    } else {
        // parent
        close(infds[0]);
        close(outfds[1]);
        close(flagfds[1]);
        child->pid = pid;
        child->in = infds[1];
        child->out = outfds[0];
        child->flag = flagfds[0];
        fcntl(child->in, F_SETFD, FD_CLOEXEC);
        fcntl(child->out, F_SETFD, FD_CLOEXEC);
        fcntl(child->flag, F_SETFD, FD_CLOEXEC);
        setbuffer(fdopen(infds[1], "w"), NULL, 0);
        setbuffer(fdopen(outfds[0], "r"), NULL, 0);
        setbuffer(fdopen(flagfds[0], "r"), NULL, 0);
    }

    return true;
}

static bool write_all(int fd, const char* buf, int n)
{
    while (n > 0) {
        int w = write(fd, buf, n);
        if (w < 0) return false;
        buf += w;
        n -= w;
    }
    return true;
}

static void set_child_last_active(Child *child)
{
    struct timespec clock_mono;
    clock_gettime(CLOCK_MONOTONIC, &clock_mono);
    child->last_active = clock_mono.tv_sec;
}

/* Write anything written timelily on `in` to `out`.
 * Streaming will cease if there is nothing to read on `in` for `read_timeout` 
 * ms, or after `pause_timeout` ms if something has been read, or after '<' is 
 * read from `flag` without a subsequent '>'.
 * Return:
 *  -1 on read error
 *  0 on HUP
 *  2 if a command link ("=> ?") was output and detect_links is true
 *  1 otherwise.
 */
static int stream_text(int in, int flag, int out,
        bool escape_pre,
        bool escape_all,
        bool detect_links,
        int munge_links_serial,
        bool *child_reading,
        int read_timeout, int pause_timeout) {
    char buf[256];
    struct pollfd pfd[2] = { { in, POLLIN | POLLHUP, 0 }, {flag, POLLIN, 0 } };
    int backticks = 0;
    char escape = 0;
    int link = 0;
    int link_found = false;
    bool read_something = false;

    /* Note we set no total maximum time or output size limit; we leave it to 
     * the user to e.g. set a ulimit to handle runaway processes. */
    while (true) {
        poll(pfd, 2, *child_reading ? 20 :
                read_something ? read_timeout : pause_timeout);

        if (pfd[0].revents & POLLIN) {
            read_something = true;
            const int r = read(in, buf, 256 - 1);
            if (r < 0) return -1;
            buf[r] = 0;

            const char *b = buf;
            while (*b) {
                if ((escape_pre || escape_all) && backticks >= 0) {
                    if (*b == '`') {
                        escape = 0;
                        ++backticks;
                        if (backticks == 3) {
                            write(out, " ```", 4);
                            backticks = -1;
                        }
                        ++b;
                        continue;
                    } else while (--backticks >= 0) write(out, "`", 1);
                }

                if ((detect_links && !link_found) || munge_links_serial > 0) {
                    if ((link == 0 && *b == '=') ||
                        (link == 1 && *b == '>') ||
                        (link == 2 && *b == '?')) {
                        link++;
                        if (link == 3) link_found = true;
                    } else if (link == 2 && (*b == ' ' || *b == '\t')) {
                    } else if (link == 3) {
                        if (munge_links_serial > 0 && *b != '!') {
                            // Replace "=> ?foo" at start of a line 
                            // with "=> ?!l+[serial]?foo",
                            // but leave "=> ?!" alone.
                            dprintf(out, "!l+%d?", munge_links_serial);
                        }
                        link = -1;
                    } else if (*b == '\n') {
                        link = 0;
                    } else {
                        link = -1;
                    }
                } else if (escape_all && escape > 0) {
                    if (escape == '\n') {
                        if (*b == '#' || *b == '>') {
                            write(out, " ", 1);
                        } else if (*b == '=' || *b == '*') {
                            escape = *b;
                            ++b;
                            continue;
                        }
                    } else {
                        if ((escape == '=' && *b == '>')
                                || (escape == '*' && *b == ' ')) {
                            write(out, " ", 1);
                        }
                        write(out, &escape, 1);
                    }
                    escape = 0;
                }

                write(out, b, 1);
                ++b;
            }
        } else if (pfd[1].revents & POLLIN) {
            const int r = read(flag, buf, 256);
            for (int i = 0; i < r; ++i ) {
                if (buf[i] == '<') *child_reading = true;
                if (buf[i] == '>') *child_reading = false;
            }
        } else break;
    }
    while (--backticks >= 0) write(out, "`", 1);
    if (escape > 0 && escape != '\n') write(out, &escape, 1);
    return pfd[0].revents & POLLHUP
        ? 0
        : link_found ? 2 : 1;
}

#define put(s) write_all(socket, s, strlen(s))
#define putn(s,n) write_all(socket, s, n)

static Child *get_session(State *state, const Request_Info *request_info, int socket)
{

    if (request_info->tls_client_hash == NULL) {
        put("60 Client certificate required\r\n");
        return NULL;
    }

    if (request_info->path_info == NULL || strlen(request_info->path_info) <= 1) {
        if (state->single_session) {
            for (int i = 0; i < state->num_children; ++i) {
                Child *const c = &state->children[i];
                if (c->mutex != NULL && pthread_mutex_trylock(c->mutex) != 0) continue;
                bool found = (c->exists &&
                        0 == strncmp(c->owner, request_info->tls_client_hash, 64));
                if (c->mutex != NULL) pthread_mutex_unlock(c->mutex);
                if (found) {
                    put("30 ");
                    put(request_info->script_path);
                    put("/");
                    putn(c->sess_id, SESSION_ID_LEN);
                    put("\r\n");
                    return NULL;
                }
            }
        }

        Child *slot = NULL;
        uint64_t last_active = UINT64_MAX;
        for (int i = 0; i < state->num_children; ++i) {
            Child *const c = &state->children[i];
            if (c->mutex != NULL && pthread_mutex_trylock(c->mutex) != 0) continue;
            if (c->exists) {
                if (last_active > c->last_active) {
                    slot = c;
                    last_active = c->last_active;
                }
            } else if (slot == NULL || last_active < UINT64_MAX) slot = c;
            if (c->mutex != NULL) pthread_mutex_unlock(c->mutex);
        }

        if (slot == NULL || (last_active < UINT64_MAX && state->num_children < state->max_children)) {
            slot = &state->children[state->num_children++];
        }

        Child *const child = slot;
        if (child->mutex != NULL) pthread_mutex_lock(child->mutex);

        if (child->exists) {
            // TODO: would be nice to queue a regretful message for the owner 
            // of the child we're killing...
            close(child->in);
            close(child->out);
            close(child->flag);
            kill(child->pid, 9);
            child->exists = false;
        }

        memset(child, 0, sizeof(Child));

        strncpy(child->owner, request_info->tls_client_hash, 64);
        for (int i = 0; i < SESSION_ID_LEN; ++i) {
            child->sess_id[i] = 'A' + random()%26 + (random()%2 ? ('a'-'A') : 0);
        }

        if (!spawn(state->command, state->args, request_info->query_string_decoded, child, socket)) {
            put("40 Spawn failure.\r\n");
            if (child->mutex != NULL) pthread_mutex_unlock(child->mutex);
            return NULL;
        }

        if (child->mutex == NULL) {
            child->mutex = malloc(sizeof(pthread_mutex_t));
            if (child->mutex == NULL) {
                put("40 Spawn failure (malloc).\r\n");
                return NULL;
            }

            if (pthread_mutex_init(child->mutex, NULL) != 0) {
                put("40 Spawn failure (mutex_init).\r\n");
                free(child->mutex);
                child->mutex = NULL;
                return NULL;
            }

            // Note: we never destroy the mutex, because we never know that it 
            // would be safe to do so.

            pthread_mutex_lock(child->mutex);
        }

        child->exists = true;
        child->newborn = true;
        set_child_last_active(child);

        child->no_link = state->no_link;
        child->always_link = state->always_link;

        put("30 ");
        put(request_info->script_path);
        put("/");
        putn(child->sess_id, SESSION_ID_LEN);
        put("\r\n");
        pthread_mutex_unlock(child->mutex);
        return NULL;
    }

    if (0 == strncmp(request_info->path_info, "/list", strlen(request_info->path_info))) {
        put("20 text/gemini\r\n");
        bool found = false;
        for (int i = 0; i < state->num_children; ++i) {
            Child *const c = &state->children[i];
            if (c->mutex != NULL && pthread_mutex_trylock(c->mutex) != 0) continue;
            if (c->exists &&
                    0 == strncmp(c->owner, request_info->tls_client_hash, 64)) {
                if (!found) {
                    found = true;
                    put("20 text/gemini\r\n");
                }
                put("=> ");
                put(request_info->script_path);
                put("/");
                putn(c->sess_id, SESSION_ID_LEN);
                put(" Resume session\n");
            }
            if (c->mutex != NULL) pthread_mutex_unlock(c->mutex);
        }
        if (!found) put("No sessions found.\n");
        return NULL;
    }

    if (strlen(request_info->path_info) != 1+SESSION_ID_LEN) {
        put("51 Bad session id.\r\n");
        return NULL;
    }

    // drop initial '/'
    const char *sess_id = request_info->path_info + 1;

    /* Find child with this sess_id.
     * For simplicity, in particular for the mutex handling, we use a static 
     * array of children rather than allocating dynamically, and don't sort. 
     * This could be optimised. */
    Child *child = NULL;
    for (int i = 0; child == NULL && i < state->num_children; ++i) {
        Child *const c = &state->children[i];
        if (c->mutex != NULL && pthread_mutex_trylock(c->mutex) != 0) continue;
        if (c->exists &&
                0 == strncmp(c->sess_id, sess_id, SESSION_ID_LEN)) {
            child = c;
        }
        if (c->mutex != NULL) pthread_mutex_unlock(c->mutex);
    }

    if (child == NULL) {
        put("20 text/gemini\r\nSession not found.\n=> ");
        put(request_info->script_path);
        put(" Start new session\n");
        return NULL;
    }

    pthread_mutex_lock(child->mutex);
    const char* owner = child->owner;
    pthread_mutex_unlock(child->mutex);

    if (0 != strncmp(owner, request_info->tls_client_hash, 64)) {
        put("61 Wrong certificate for session.\r\n");
        return NULL;
    }

    return child;
}

static void do_command(const State* state, Child *child, const char* q, int socket) {
    if (*q == '!') {
        ++q;
        if (*q == '?') {
            put("10\r\n");
            return;
        } else if (0 == strncmp(q, "help", strlen(q))) {
            put("20 text/gemini\r\n");
            put("An input line not beginning with '!' will be passed to the process.\n");
            put("\n");
            put("# gemrepl meta commands\n");
            put("=> ?!help !help: This help\n");
            put("=> ?!kill !kill: kill process\n");
            if (state->format != raw) {
                put("=> ?!nolink !nolink: suppress input link\n");
                put("=> ?!showlink !showlink: show input link\n");
                put("=> ?!plain !plain: use text/plain for responses\n");
                put("=> ?!gemtext !gemtext: use text/gemini for responses (default)\n");
            }
            put("=> ?!C !C: pass ^C (SIGINT) to process\n");
            put("=> ?!? !?: Prompt for input\n");
            put("=> ?!! !!: Literal '!'\n");
            return;
        } else if (0 == strncmp(q, "kill", strlen(q))) {
            kill(-child->pid, SIGKILL);
            q += strlen(q);
        } else if (0 == strncmp(q, "C", strlen(q))) {
            kill(-child->pid, SIGINT);
            q += strlen(q);
        } else if (0 == strncmp(q, "nolink", strlen(q))) {
            // TODO: might be better to have this be a permanent option 
            // attached to the cert rather than the child.
            child->no_link = true;
            put("20 text/gemini\r\n");
            put("Input links disabled.\n");
            put("=> ?!showlink Re-enable input links\n");
            return;
        } else if (0 == strncmp(q, "showlink", strlen(q))) {
            child->no_link = false;
            put("20 text/gemini\r\n");
            put("Input links enabled.\n");
            put("=> ?!? Input command\n");
            return;
        } else if (0 == strncmp(q, "plain", strlen(q))) {
            child->plain = true;
            put("20 text/gemini\r\n");
            put("Plaintext mode enabled.\n");
            put("=> ?!gemtext Re-enable gemtext\n");
            return;
        } else if (0 == strncmp(q, "gemtext", strlen(q))) {
            child->plain = false;
            put("20 text/gemini\r\n");
            put("Gemtext mode enabled.\n");
            put("=> ?!? Input command\n");
            return;
        } else if (0 == strncmp(q, "l+", strlen("l+"))) {
            q += 2;
            if (child->serial == atoi(q)) {
                while (*q && *q != '?') {
                    q++;
                }
                if (*q) q++;
            } else {
                put("20 text/gemini\r\n");
                put("That link is stale.\n");
                put("=> ?!? Input command\n");
                return;
            }
        } else if (*q != '!') {
            put("40 Unknown gemrepl meta-command (use '!!' for a literal '!')\r\n");
            return;
        }
    } else if (strchr(q, '\n') != NULL) {
        put("40 Input may not include embedded newlines\r\n");
        return;
    }

    child->serial++;

    if (state->format != raw) {
        if (child->plain) put("20 text/plain\r\n");
        else put("20 text/gemini\r\n");

        if (child->newborn) {
            put("[gemrepl: child spawned. Input \"!help\" for meta-commands]\n");
        }

        if (child->always_link && !child->plain) {
            put("=> ?!? Input command\n");
        }
    }

    if (!child->newborn) kill(-child->pid, SIGCONT);

    int qlen = strlen(q);
    if (!child->newborn) {
        bool succ = (write(child->in, q, qlen) == qlen
                && write(child->in, "\n", 1) == 1);
        if (!succ) {
            put("[gemrepl: error when writing to child]\n");
        }

        child->reading = false;
    }

    if (state->format == pre && !child->plain) put("```\n");
    const int succ = stream_text(child->out, child->flag, socket,
            state->format == pre && !child->plain,
            state->format == unwrapped && !child->plain,
            state->format == gemtext,
            state->format == gemtext ? child->serial : 0,
            &child->reading,
            state->read_timeout,
            state->pause_timeout);
    if (state->format == pre && !child->plain) put("\n```\n");

    if (succ < 0) put("[gemrepl: error when reading from child]\n");
    else if (succ == 0) {
        // got HUP; sleep briefly to give child a chance to exit
        usleep(50000);
    } else if (succ == 1 && !child->no_link && !child->always_link) {
        put("=> ?!? Input command\n");
    }

    set_child_last_active(child);
    child->newborn = false;

    if (waitpid(child->pid, NULL, WNOHANG) == child->pid) {
        if (state->format != raw) {
            put("[gemrepl: child process terminated]\n");
            put("=> . Start new session\n");
        }
        close(child->in);
        close(child->out);
        close(child->flag);
        child->exists = false;
    } else {
        kill(-child->pid, SIGSTOP);
    }
}

// Wrap arguments of do_command into a single struct, for use with 
// pthread_create.
typedef struct Do_Command_Arg {
    const State* state;
    Child *child;
    const char* q;
    int socket;
} Do_Command_Arg;

static void *do_command_thread(void *object)
{
    Do_Command_Arg *arg = (Do_Command_Arg *)object;

    pthread_mutex_lock(arg->child->mutex);
    do_command(arg->state, arg->child, arg->q, arg->socket);
    pthread_mutex_unlock(arg->child->mutex);

    close(arg->socket);
    free(arg);
    return NULL;
}

void respond(void *object, const Request_Info *request_info, int socket)
{
    State *state = (State *)object;

    Child *child = get_session(state, request_info, socket);

    if (child == NULL) {
        close(socket);
        return;
    }

    Do_Command_Arg *arg = malloc(sizeof(Do_Command_Arg));
    if (arg == NULL) {
        close(socket);
        return;
    }
    arg->state = state;
    arg->child = child;
    arg->q = request_info->query_string_decoded;
    arg->socket = socket;

    pthread_t tid;
    pthread_create(&tid, NULL, do_command_thread, arg);
}

/* How long in ms to wait for child to output something */
#define DEF_READ_TIMEOUT 3000

/* How long in ms child can pause between writes before we consider it to have 
 * finished writing */
#define DEF_PAUSE_TIMEOUT 300

static void usage()
{
    printf("Usage: gemrepl [OPTION]... -s PATH COMMAND [ARG]...\n");
    printf("  -h      --help             This help\n");
    printf("  -s PATH --socket=PATH      Path for socket file, which will be created\n");
    printf("  -m NUM  --max-children=NUM Max concurrent children to spawn (%d)\n", MAX_CHILDREN);
    printf("  -t MS   --read-timeout=MS  Time to wait for child to start writing (%d)\n", DEF_READ_TIMEOUT);
    printf("  -T MS   --pause-timeout=MS Silence period after which child is paused (%d)\n", DEF_PAUSE_TIMEOUT);
    printf("  -S      --synchronous      Disable timeouts. Use fd 3 instead (see docs).\n");
    printf("  -l      --always-link      Always write input link.\n");
    printf("  -L      --no-link          Never write input link.\n");
    printf("  -1      --single-session   Allow only one session per user.\n");
    printf("  -f FMT  --format=FMT       Format of output of command. Possible formats:\n");
    printf("                   gemtext: text/gemini (default)\n");
    printf("                   pre: preformatted text\n");
    printf("                   unwrapped: plain text without hard wrapping\n");
    printf("                   raw: gemini protocol output, including response headers\n");

}

/* state as global variable, so we can clean up on termination */
State state = {};

static void cleanup(int sig) {
    for (int i = 0; i < state.num_children; ++i) {
        Child *const c = &state.children[i];
        if (c->exists) {
            close(c->in);
            close(c->out);
            close(c->flag);
            kill(-c->pid, SIGKILL);
            waitpid(c->pid, NULL, 0);
        }
    }
    exit(1);
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        usage();
        exit(1);
    }

    state.max_children = MAX_CHILDREN;
    state.read_timeout = DEF_READ_TIMEOUT;
    state.pause_timeout = DEF_PAUSE_TIMEOUT;
    state.format = gemtext;

    const struct option longoptions[] =
        { { "help", 0, NULL, 'h' }
        , { "socket", 1, NULL, 's' }
        , { "format", 1, NULL, 'f' }
        , { "always-link", 1, NULL, 'l' }
        , { "no-link", 1, NULL, 'L' }
        , { "max-children", 1, NULL, 'm' }
        , { "read-timeout", 1, NULL, 't' }
        , { "pause-timeout", 1, NULL, 'T' }
        , { "synchronous", 0, NULL, 'S' }
        , { "single-session", 0, NULL, '1' }
        , { 0,0,0,0 }
        };
    int o;
    const char *socketname = NULL;
    while (-1 != (o = getopt_long(argc, argv, "+1hs:f:lLm:t:T:S", longoptions, NULL))) {
        switch (o) {
            case 'h':
            case '?':
                usage();
                exit((o=='?'));
            case 's':
                socketname = optarg;
                break;
            case '1':
                state.single_session = true;
                break;
            case 'f':
                if (0 == strcmp(optarg, "gemtext")) state.format=gemtext;
                else if (0 == strcmp(optarg, "pre")) state.format=pre;
                else if (0 == strcmp(optarg, "unwrapped")) state.format=unwrapped;
                else if (0 == strcmp(optarg, "raw")) state.format=raw;
                else {
                    printf("Unknown format.\n");
                    exit(1);
                }
                break;
            case 'l':
                state.always_link = true;
                break;
            case 'L':
                state.no_link = true;
                break;
            case 'm':
                state.max_children = atoi(optarg);
                if (state.max_children <= 0 || state.max_children > MAX_CHILDREN) {
                    printf("Bad value for max children.\n");
                    printf("You may need to increase MAX_CHILDREN in the source.\n");
                    exit(1);
                }
                break;
            case 't':
                state.read_timeout = atoi(optarg);
                break;
            case 'T':
                state.pause_timeout = atoi(optarg);
                break;
            case 'S':
                state.read_timeout = -1;
                state.pause_timeout = -1;
                break;
        }
    }

    if (argv[optind] == NULL || socketname == NULL) {
        usage();
        exit(1);
    }

    state.command = argv[optind];
    state.args = &argv[optind];

    srandom(time(NULL));

    struct sigaction act = {};
    act.sa_handler = cleanup;
    sigaction(SIGTERM, &act, NULL);
    sigaction(SIGINT, &act, NULL);
    act.sa_handler = SIG_IGN;
    sigaction(SIGPIPE, &act, NULL);

    runSCGI(socketname, respond, &state);
}
