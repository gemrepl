CC=cc
CFLAGS=-Wall -g

gemrepl: gemscgi.o main.o
	$(CC) $(CFLAGS) -lpthread -o $@ $^

%.o: %.c *.h
	$(CC) $(CFLAGS) -c $<

gemscgi_example: gemscgi.o gemscgi_example.o
	$(CC) $(CFLAGS) -o $@ $^

gemscgi_wrap: gemscgi.o gemscgi_wrap.o
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm gemrepl gemscgi_example gemscgi.o gemscgi_example.o main.o
